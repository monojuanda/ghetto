/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Tamayo B
 */
@Entity
@Table(name = "inv_mujer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InvMujer.findAll", query = "SELECT i FROM InvMujer i")
    , @NamedQuery(name = "InvMujer.findByItem", query = "SELECT i FROM InvMujer i WHERE i.item = :item")
    , @NamedQuery(name = "InvMujer.findByNombre", query = "SELECT i FROM InvMujer i WHERE i.nombre = :nombre")
    , @NamedQuery(name = "InvMujer.findByDescripcion", query = "SELECT i FROM InvMujer i WHERE i.descripcion = :descripcion")
    , @NamedQuery(name = "InvMujer.findByPrecio", query = "SELECT i FROM InvMujer i WHERE i.precio = :precio")
    , @NamedQuery(name = "InvMujer.findByColeccion", query = "SELECT i FROM InvMujer i WHERE i.coleccion = :coleccion")
    , @NamedQuery(name = "InvMujer.findByHabilitado", query = "SELECT i FROM InvMujer i WHERE i.habilitado = :habilitado")})
public class InvMujer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "item")
    private Integer item;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "precio")
    private Integer precio;
    @Column(name = "coleccion")
    private String coleccion;
    @Lob
    @Column(name = "imagen")
    private byte[] imagen;
    @Column(name = "Habilitado")
    private String habilitado;

    public InvMujer() {
    }

    public InvMujer(Integer item) {
        this.item = item;
    }

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public String getColeccion() {
        return coleccion;
    }

    public void setColeccion(String coleccion) {
        this.coleccion = coleccion;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String habilitado) {
        this.habilitado = habilitado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (item != null ? item.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvMujer)) {
            return false;
        }
        InvMujer other = (InvMujer) object;
        if ((this.item == null && other.item != null) || (this.item != null && !this.item.equals(other.item))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controlador.Modelo.InvMujer[ item=" + item + " ]";
    }
    
}
