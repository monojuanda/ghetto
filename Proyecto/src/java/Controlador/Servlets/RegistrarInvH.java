/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Servlets;

import Controlador.Conexion.Conectadb;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2,
        maxFileSize = 1024 * 1024 * 10,
       maxRequestSize = 1024 * 1024 * 50) 

/**
 *
 * @author juandavid
 */
public class RegistrarInvH extends HttpServlet {

    private static final String SAVE_DIR = "imagenes";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String appPath = request.getServletContext().getRealPath("");
        String savePath = appPath + File.separator + SAVE_DIR;

        String des = request.getParameter("descripcion");
        String pre = request.getParameter("precio");
        String col = request.getParameter("coleccion");
        String nom = request.getParameter("nombre");
        String Habilitado = "E";
        Part file = request.getPart("imagen");

        //String savePath = "C:\\Users\\Tamayo B\\Desktop\\Proyectos\\Ghetto\\web\\imagenes\\" + File.separator + fileName;
        //String savePath = "C:\\Users\\Tamayo B\\Desktop\\Proyectos\\Ghetto\\web\\imagenes\\" + File.separator + SAVE_DIR;
        File fileSaveDir = new File(savePath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }

        try {
            Conectadb con = new Conectadb();
            Connection c = con.conectar();
            PreparedStatement ps = c.prepareStatement("INSERT INTO Inv_hombre (nombre, descripcion, precio, coleccion, imagen, habilitado, imgNom) VALUES (?, ?, ?, ?, ?, ?, ?)");
            ps.setString(1, nom);
            ps.setString(2, des);
            ps.setString(3, pre);
            ps.setString(4, col);
            ps.setString(6, Habilitado);

            for (Part part : request.getParts()) {
                String fileName = extractFileName(file);
                // refines the fileName in case it is an absolute path
                fileName = new File(fileName).getName();
                part.write(savePath + File.separator + fileName);
                String filePath = savePath + File.separator + fileName;
                ps.setString(5, filePath);
                ps.setString(7, fileName);
            }
            ps.executeUpdate();

            System.out.println(ps);
            response.sendRedirect("Index.jsp");
        } catch (Exception slx) {
            System.out.println(slx);
            System.out.println("No");
            response.sendRedirect("RegistrarInventariosH.jsp");

        }

    }

    private String extractFileName(Part file) {
        String contentDisp = file.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        for (String s : items) {
            if (s.trim().startsWith("file")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 1);
            }

        }
        return "";
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
