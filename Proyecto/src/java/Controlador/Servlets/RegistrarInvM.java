/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Servlets;

import Controlador.Conexion.Conectadb;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@MultipartConfig(maxFileSize = 16177215)

/**
 *
 * @author Tamayo B
 */
public class RegistrarInvM extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        InputStream inputStream = null;
        String des = request.getParameter("descripcion");
        String pre = request.getParameter("precio");
        String col = request.getParameter("coleccion");
        Part file = request.getPart("imagen");
        String nom = request.getParameter("nombre");
        
         if (file != null) {
            System.out.println(file.getName());
            System.out.println(file.getSize());
            System.out.println(file.getContentType());

            inputStream = file.getInputStream();
        }
         try {
            Conectadb con = new Conectadb();
            Connection c = con.conectar();
            PreparedStatement ps = c.prepareStatement("INSERT INTO Inv_mujer (nombre, descripcion, precio, coleccion, imagen) VALUES (?, ?, ?, ?, ?)");
            ps.setString(1, nom);
            ps.setString(2, des);
            ps.setString(3, pre);
            ps.setString(4, col);
            if (inputStream != null) {
                ps.setBinaryStream(5, inputStream, (int) file.getSize());
            }

            int row = ps.executeUpdate();
            if (row > 0) {
            out.println("Inserto");

                c.close();

                response.sendRedirect("ListarInventarioM");
            } else {
                out.println("No inserto");
                c.close();
            }
            ps.execute();

            
        } catch (Exception slx) {
            System.out.println(slx);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
