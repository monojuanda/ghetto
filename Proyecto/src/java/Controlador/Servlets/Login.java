package Controlador.Servlets;

import Controlador.Conexion.Conectadb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            Conectadb c = new Conectadb();
            Connection con = c.conectar();
            Statement st = con.createStatement();

            String query = "SELECT * from Usuarios WHERE correo = '" + request.getParameter("ntext") + "' AND contraseña='" + request.getParameter("ptext") + "';";
            System.out.println(query);

            ResultSet rs = st.executeQuery(query);
            String usuario = request.getParameter("ntext");
            String contra = request.getParameter("ptext");
            if (rs.first()) {
                HttpSession sesion = request.getSession(true);
                Cookie cookie = new Cookie("usuario", usuario);
                cookie.setMaxAge(60*60);
                response.addCookie(cookie);
                sesion.setAttribute("nusuario", rs.getString(1));
                sesion.setAttribute("rusuario", rs.getString(4));
                
                String rol = rs.getString(4);
                System.out.println(rol);
                
                if (rol.equals("1")){
                response.sendRedirect("ADMIN.jsp");
                
                }else if (rol.equals("4")) {
                    response.sendRedirect("Index.jsp");
                } else {
                response.sendRedirect("Index.jsp");
            }
                               
                }
            st.close();
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
