package Controlador.Modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-03-13T23:34:08")
@StaticMetamodel(InvMujer.class)
public class InvMujer_ { 

    public static volatile SingularAttribute<InvMujer, String> descripcion;
    public static volatile SingularAttribute<InvMujer, Integer> item;
    public static volatile SingularAttribute<InvMujer, Integer> precio;
    public static volatile SingularAttribute<InvMujer, String> coleccion;
    public static volatile SingularAttribute<InvMujer, byte[]> imagen;
    public static volatile SingularAttribute<InvMujer, String> habilitado;
    public static volatile SingularAttribute<InvMujer, String> nombre;

}