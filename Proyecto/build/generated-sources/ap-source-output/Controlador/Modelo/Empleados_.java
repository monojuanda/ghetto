package Controlador.Modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-03-13T23:34:08")
@StaticMetamodel(Empleados.class)
public class Empleados_ { 

    public static volatile SingularAttribute<Empleados, String> apellidos;
    public static volatile SingularAttribute<Empleados, Date> fecha;
    public static volatile SingularAttribute<Empleados, String> tipo;
    public static volatile SingularAttribute<Empleados, String> ciudad;
    public static volatile SingularAttribute<Empleados, String> correo;
    public static volatile SingularAttribute<Empleados, String> direccion;
    public static volatile SingularAttribute<Empleados, String> identificacion;
    public static volatile SingularAttribute<Empleados, String> ocupacion;
    public static volatile SingularAttribute<Empleados, String> habilitado;
    public static volatile SingularAttribute<Empleados, String> sexo;
    public static volatile SingularAttribute<Empleados, String> telefono;
    public static volatile SingularAttribute<Empleados, String> nombres;

}