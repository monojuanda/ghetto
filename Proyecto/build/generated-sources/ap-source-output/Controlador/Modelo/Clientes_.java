package Controlador.Modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-03-13T23:34:08")
@StaticMetamodel(Clientes.class)
public class Clientes_ { 

    public static volatile SingularAttribute<Clientes, String> apellidos;
    public static volatile SingularAttribute<Clientes, Date> fecha;
    public static volatile SingularAttribute<Clientes, String> tipo;
    public static volatile SingularAttribute<Clientes, String> ciudad;
    public static volatile SingularAttribute<Clientes, String> correo;
    public static volatile SingularAttribute<Clientes, String> direccion;
    public static volatile SingularAttribute<Clientes, String> departamento;
    public static volatile SingularAttribute<Clientes, String> identificacion;
    public static volatile SingularAttribute<Clientes, String> habilitado;
    public static volatile SingularAttribute<Clientes, String> sexo;
    public static volatile SingularAttribute<Clientes, String> telefono;
    public static volatile SingularAttribute<Clientes, String> nombres;

}