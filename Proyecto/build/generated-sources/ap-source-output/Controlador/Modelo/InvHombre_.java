package Controlador.Modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-03-13T23:34:08")
@StaticMetamodel(InvHombre.class)
public class InvHombre_ { 

    public static volatile SingularAttribute<InvHombre, String> descripcion;
    public static volatile SingularAttribute<InvHombre, Integer> item;
    public static volatile SingularAttribute<InvHombre, Integer> precio;
    public static volatile SingularAttribute<InvHombre, String> imgNom;
    public static volatile SingularAttribute<InvHombre, String> coleccion;
    public static volatile SingularAttribute<InvHombre, String> imagen;
    public static volatile SingularAttribute<InvHombre, String> habilitado;
    public static volatile SingularAttribute<InvHombre, String> nombre;

}