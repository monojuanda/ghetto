<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>         
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap-4-navbar.css" type="text/css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <title>Ghetto</title>
    </head>

    <body class="fondo1">
         
        <header class="banner">
        </header>
        <%
            HttpSession se = request.getSession(true);
            String rol = "invitado";
            String nu = "Ingresar";
            String msg = "Registrar";
            if (se.getAttribute("rusuario") == null) {
                rol = "invitado";
            } else {
                rol = (String) se.getAttribute("rusuario");
                nu = (String) se.getAttribute("nusuario");
                msg = "";
            }
            if (rol.equals("1")) {

        %>
        
        <nav class="navbar navbar-expand-lg navbar-dark mynavbar" data-toggle="sticky-onscroll">
            <a class="navbar-brand" href="ADMIN.jsp">Ghetto</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">

                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item"> <a  href="Logout" class="nav-link"><%= nu %></a></li>
                    
                </ul>
               </div>
        </nav>
        
        <div class="container">
            <!--Empleados-->
            <div class="row">    
                <div class="col-lg-12" style="margin-top:45px;">
                    <div class="card-deck">
                        <div class="card mb-5">
                            <div class="card-body text-center">
                                <a href="RegistrarEmpleados.jsp" class="btn separador">
                                    <span title="Registrar Empleados">
                                          <span class="fas fa-users fa-6x color"></span>
                                    </span>
                                </a>
                                <a href="ListarEmpleado" class="btn">
                                    <span title="Listar Empleados">
                                        <span class="fas fa-list-ul fa-6x color"></span>
                                    </span>
                                </a>
                                <div class="space">
                                    <h5 class="card-title">Empleados</h5>
                                </div>
                            </div>
                        </div>
                        
                        <!-- CLientes -->
                        <div class="card mb-5">
                            <div class="card-body text-center">
                                <a href="RegistrarClientes.jsp" class="btn separador">
                                    <span title="Registrar Clientes">
                                        <span class="fas fa-user fa-6x color"></span>
                                    </span>
                                </a>
                                <a href="ListarCliente" class="btn">
                                    <span title="Listar Clientes">
                                        <span class="fas fa-list-ul fa-6x color"></span>
                                    </span>
                                </a>
                                <div class="space">
                                    <h5 class="card-title">Clientes</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-5">
                             <div class="card-body text-center">
                                 <a href="RegistrarVentas.jsp" class="btn separador">
                                     <span title="Registrar ventas">
                                         <span class="fas fa-hand-holding-usd fa-6x color"></span>
                                     </span>
                                 </a>
                                 <a href="ListarVenta" class="btn">                                   
                                    <span title="Listar ventas">
                                        <span class="fas fa-list-ul fa-6x color"></span>
                                    </span>
                                 </a>
                                 <div class="space">
                                 <h5 class="card-title">Ventas</h5>
                                 </div>
                             </div>
                         </div>
                    </div>
                    <div class="card-deck">
                        <div class="card mb-5">
                            <div class="card-body text-center">
                                <a href="RegistrarPedidos.jsp" class="btn separador">
                                    <span title="Registrar Pedidos">
                                        <span class="fas fa-cart-plus fa-6x color"></span>
                                    </span>
                                </a>
                                <a href="ListarPedido" class="btn">
                                    <span title="Listar Pedidos">
                                        <span class="fas fa-list-ul fa-6x color"></span>
                                    </span>
                                </a>
                                <div class="space">
                                    <h5 class="card-title">Pedidos</h5>
                                </div>
                            </div>
                        </div>

                        <div class="card mb-5">
                            <div class="card-body text-center ">
                                <a href="RegistrarInventariosH.jsp" class="btn separador">
                                    <span title="Registrar Inventario de Hombre">
                                        <span class="fas fa-male fa-6x color"></span>
                                    </span>
                                </a>
                                <a href="ListarInventarioH" class="btn ">
                                    <span title="Listar Inventario de Hombre">
                                        <span class="fas fa-list-ul fa-6x color"></span>
                                    </span>
                                </a>
                                <div class="space">
                                    <h5 class="card-title">Inventario Hombre</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-5">
                            <div class="card-body text-center ">
                                <a href="RegistrarInventariosM.jsp" class="btn separador">
                                    <span title="Registrar Inventario de Mujer">
                                        <span class="fas fa-female fa-6x color"></span>
                                    </span>    
                                </a>
                                <a href="ListarInventarioM" class="btn ">
                                    <span title="Listar Inventario de Mujer">
                                        <span class="fas fa-list-ul fa-6x color"></span>
                                    </span>
                                </a>
                                <div class="space">
                                    <h5 class="card-title">Inventario Mujer</h5>
                                </div>
                            </div>
                        </div>
                        <!--<div class="card mb-5">-->
                        <!--    <div class="card-body text-center">-->
                        <!--        <a href="#" class="btn"><span class="fas fa-file-invoice-dollar fa-6x color"></span></a>-->
                        <!--        <a href="#" class="btn"><span class="fas fa-list-ul fa-6x color"></span></a>-->
                        <!--        <div class="space">-->
                        <!--        <h5 class="card-title">Facturación</h5>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->

                    </div>
                    <div class="card-deck">
                        <div class="card mb-5">
                            <div class="card-body text-center">
                                <a href="RegistrarProveedores.jsp" class="btn separador"><span class="fas fa-truck fa-6x color"></span></a>
                                <a href="ListarProveedor" class="btn"><span class="fas fa-list-ul fa-6x color"></span></a>
                                <div class="space">
                                    <h5 class="card-title">Proveedores</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-5">
                            <div class="card-body text-center">
                                <a href="Permisos.jsp" class="btn"><span class="fas fa-check-square fa-6x color"></span></a>
                                <div class="space">
                                    <h5 class="card-title">Permisos</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-5">
                            <div class="card-body text-center">
                                <a href="Logs.jsp" class="btn"><img src="imagenes/Logs.png" class="color" alt=""></span></a>
                                <div class="space">
                                    <h5 class="card-title">Logs</h5>
                                </div>
                            </div>
                        </div>
                        <!--<div class="card mb-5">-->
                        <!--    <div class="card-body text-center">-->
                        <!--        <a href="#" class="btn"><span class="fas fa-envelope fa-6x color"></span></a>-->
                        <!--        <a href="#" class="btn"><span class="fas fa-list-ul fa-6x color"></span></a>-->
                        <!--        <div class="space">-->
                        <!--        <h5 class="card-title">Mensajería</h5>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <!--<div class="card mb-5">-->
                        <!--    <div class="card-body text-center">-->
                        <!--        <a href="#" class="btn"><span class="far fa-credit-card fa-6x color"></span></a>-->
                        <!--        <a href="#" class="btn"><span class="fas fa-list-ul fa-6x color"></span></a>-->
                        <!--        <div class="space">-->
                        <!--        <h5 class="card-title">Pagos</h5>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->
                    </div>

                </div>
            </div>
        </div>
        <%        } else if (rol.equals("2")) {
        %>
        <div class="container">  
            <div class="row">
                <div class="col-lg-12" style="margin-top:45px;">

                    <div class="card-deck">
                        <div class="card mb-5">
                    
                             <div class="card-body text-center">
                                 <span title="Listar Ventas" <a href="ListarVentas.jsp" class="btn"><span class="fas fa-list-ul fa-6x color"></span></a></span>
                                 <div class="space">
                                 <h5 class="card-title">Ventas</h5>
                                 </div>
                             </div>
                         </div> 
                        <div class="card mb-5">
                            <div class="card-body text-center">
                                <a href="RegistrarPedidos.jsp" class="btn separador"><span class="fas fa-cart-plus fa-6x color"></span></a>
                                <a href="ListarPedido" class="btn"><span class="fas fa-list-ul fa-6x color"></span></a>
                                <div class="space">
                                    <h5 class="card-title">Pedidos</h5>
                                </div>
                            </div>
                        </div>

                       <!-- <div class="card mb-5">
                            <div class="card-body text-center ">
                                <a href="RegistrarInventariosH.jsp" class="btn separador"><span class="fas fa-male fa-6x color"></span></a>
                                <a href="ListarInventarioH" class="btn "><span class="fas fa-list-ul fa-6x color"></span></a>
                                <div class="space">
                                    <h5 class="card-title">Inventario Hombre</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-5">
                            <div class="card-body text-center ">
                                <a href="RegistrarInventariosM.jsp" class="btn separador"><span class="fas fa-female fa-6x color"></span></a>
                                <a href="ListarInventarioM" class="btn "><span class="fas fa-list-ul fa-6x color"></span></a>
                                <div class="space">
                                    <h5 class="card-title">Inventario Mujer</h5>
                                </div>
                            </div>
                        </div> -->
                        <!--<div class="card mb-5">-->
                        <!--    <div class="card-body text-center">-->
                        <!--        <a href="#" class="btn"><span class="fas fa-file-invoice-dollar fa-6x color"></span></a>-->
                        <!--        <a href="#" class="btn"><span class="fas fa-list-ul fa-6x color"></span></a>-->
                        <!--        <div class="space">-->
                        <!--        <h5 class="card-title">Facturación</h5>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->

                    </div>
                </div>
            </div>
        </div>
        <% }%>   
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap-4-navbar.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/Scroll.js"></script>
    </body>



</html>
