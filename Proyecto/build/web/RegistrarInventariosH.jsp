<%-- 
    Document   : RegistrarInventariosH
    Created on : 13/11/2018, 07:28:44 PM
    Author     : Tamayo B
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap-4-navbar.css" type="text/css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <title>Ghetto</title>
    </head>

    <body class="fondo1">
        <header class="banner">
        </header>
        <%@include file="Navbar.jsp" %>
        
        <div class="container">
            <div class="form">
                <h2 class="myh2">Registrar Inventario de Hombre</h2>
                <form action="RegistrarInvH" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" id="ite" name="item">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="n" class="col-sm-2 col-form-label l-sb">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="n" placeholder="Ingresar nombre" name="nombre">
                        </div>
                    </div> 
                    
                    <div class="form-group row">
                        <label for="text" class="col-sm-2 col-form-label l-sb">Descripción:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="text" rows="3" placeholder="Ingresar descripción"  name="descripcion"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ca" class="col-sm-2 col-form-label l-sb">Precio</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="ca" placeholder="Ingresar precio" name="precio">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="correo" class="col-sm-2 col-form-label l-sb">Colección:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="correo" placeholder="Ingresar colección"  name="coleccion">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="img" class="col-sm-2 col-form-label l-sb">Imagen</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="img" name="imagen">
                        </div>
                    </div>
                    <div class="button-principal">
                        <button type="submit" class="btn btn-color">Registar Inventario</button>
                    </div>
                </form>
            </div>
        </div>

        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap-4-navbar.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/Scroll.js"></script>
    </body>



</html>

