package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/Navbar.jsp");
    _jspx_dependants.add("/Footer.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("\r\n");
      out.write("<html lang=\"en\" dir=\"ltr\">\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/fontawesome-all.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/Footer.css\">\r\n");
      out.write("        <title>Ghetto</title>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <header class=\"banner\">\r\n");
      out.write("        </header>\r\n");
      out.write("       \r\n");
      out.write("        ");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap-4-navbar.css\" type=\"text/css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/fontawesome-all.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/jquery.bootstrap4.css\" type=\"text/css\" />\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/hombre.css\" type=\"text/css\">\r\n");
      out.write("\r\n");
      out.write("        <title>Ghetto</title>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body class=\"fondo1\">\r\n");
      out.write("\r\n");
      out.write("        <nav class=\"navbar navbar-expand-lg navbar-dark mynavbar\" data-toggle=\"sticky-onscroll\">\r\n");
      out.write("            <a class=\"navbar-brand\" href=\"Index.jsp\">Ghetto</a>\r\n");
      out.write("            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("                <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("            </button>\r\n");
      out.write("            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n");
      out.write("                <ul class=\"navbar-nav mx-auto\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Hombre\">HOMBRE</a> </li>\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Mujer.jsp\">MUJER</a> </li>\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Nosotros.jsp\">EL BARRIO</a> </li>\r\n");
      out.write("\r\n");
      out.write("                    ");

                        HttpSession se = request.getSession(true);
                        String rol = "invitado";
                        String nu = "Ingresar";
                        String msg = "Registrar";
                        if (se.getAttribute("rusuario") == null) {
                            rol = "3";
                        } else {
                            rol = (String) se.getAttribute("rusuario");
                            nu = (String) se.getAttribute("nusuario");
                            msg = "";
                        }
                        if (rol.equals("1")) {

                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Empleados </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarEmpleados.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarEmpleado\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Clientes </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarClientes.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarCliente\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Pedidos </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarPedidos.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarPedido\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Ventas </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarVentas.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarVenta\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("                            Inventarios\r\n");
      out.write("                        </a>\r\n");
      out.write("                        <ul class=\"dropdown-menu m\" aria-labelledby=\"navbarDropdownMenuLink\">\r\n");
      out.write("                            <li><a class=\"dropdown-item d dropdown-toggle\" href=\"#\">Hombres</a>\r\n");
      out.write("                                <ul class=\"dropdown-menu m\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"RegistrarInventariosH.jsp\">Registrar</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"ListarInventarioH\">Listar</a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            <li><a class=\"dropdown-item d dropdown-toggle\" href=\"#\">Mujeres</a>\r\n");
      out.write("                                <ul class=\"dropdown-menu m\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"RegistrarInventariosM.jsp\">Registrar</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"ListarInventarioM\">Listar</a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <!--<li class=\"nav-item dropdown\">-->\r\n");
      out.write("                    <!--    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Facturaciones </a>-->\r\n");
      out.write("                    <!--    <div class=\"dropdown-menu m\">-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Registrar</a>-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Listar</a>-->\r\n");
      out.write("                    <!--    </div>-->\r\n");
      out.write("                    <!--</li>-->\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Proveedores </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarProveedores.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarProveedor\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Contactos </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"Contacto.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarContacto\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <!--<li class=\"nav-item dropdown\">-->\r\n");
      out.write("                    <!--    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Mensajería </a>-->\r\n");
      out.write("                    <!--    <div class=\"dropdown-menu m\">-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Registrar</a>-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Listar</a>-->\r\n");
      out.write("                    <!--    </div>-->\r\n");
      out.write("                    <!--</li>-->\r\n");
      out.write("                    <!--<li class=\"nav-item dropdown\">-->\r\n");
      out.write("                    <!--    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Pagos </a>-->\r\n");
      out.write("                    <!--    <div class=\"dropdown-menu m\">-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Registrar</a>-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Listar</a>-->\r\n");
      out.write("                    <!--    </div>-->\r\n");
      out.write("                    <!--</li>-->\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Audirotías </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"Permisos.jsp\">Permisos</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"Logs.jsp\">Logs</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    ");
                     
                        } else if (rol.equals("2")) {
                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Pedidos </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarPedidos.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarPedido\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Ventas </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">                            \r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarVentas.jsp\">Listar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarVenta\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    ");

                    } else if (rol.equals("3")) {

                    
      out.write("\r\n");
      out.write("                    <i class=\"fas fa-shopping-cart icon-cart\">\r\n");
      out.write("                        <div class=\"badge badge-color\" id=\"badge-cart\">0</div>\r\n");
      out.write("                    </i>\r\n");
      out.write("                    ");
                    } else if (rol.equals("4")) {

                    
      out.write("\r\n");
      out.write("                    <i class=\"fas fa-shopping-cart icon-cart\">\r\n");
      out.write("                        <div class=\"badge badge-color\" id=\"badge-cart\">0</div>\r\n");
      out.write("                    </i>\r\n");
      out.write("                    ");
                        }
                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                </ul>\r\n");
      out.write("\r\n");
      out.write("                ");

                    if (se.getAttribute("rusuario") == null) {
                
      out.write("\r\n");
      out.write("                <ul class=\"navbar-nav\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModal1\"> Ingresar  <span class=\"fas fa-sign-in-alt \"></span> </a></li>\r\n");
      out.write("\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link myitem\" href=\"#\" data-toggle=\"modal\" data-target=\"#myModal2\">Registrar</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                ");

                } else if (se.getAttribute("4") == null){
                
      out.write("\r\n");
      out.write("                <ul class=\"navbar-nav\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"#\"> Mis pedidos <span class=\"fas fas fa-dolly \"></span> </a></li>\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Logout\"> ");
      out.print( nu);
      out.write("</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                ");
     
                    } else {
                
      out.write("\r\n");
      out.write("                  \r\n");
      out.write("                <ul class=\"navbar-nav\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Logout\"> ");
      out.print( nu);
      out.write("</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                ");

                    }
                
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("        </nav>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <!--Modal de ingresar-->\r\n");
      out.write("        <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\r\n");
      out.write("            <div class=\"modal-dialog\">\r\n");
      out.write("                <div class=\"modal-content mc\">\r\n");
      out.write("                    <div class=\"modal-header \">\r\n");
      out.write("                        <button type=\"button\" class=\"close fas fa-times\" data-dismiss=\"modal\"></button>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"modal-body\" style=\"padding:40px 50px;\">\r\n");
      out.write("                        <form action=\"Login\">\r\n");
      out.write("\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"correo\">Correo electrónico:</label>\r\n");
      out.write("                                <input type=\"email\" class=\"form-control\" id=\"correo\" placeholder=\"Ingresar correo electrónico\" name=\"ntext\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"con\">Contraseña:</label>\r\n");
      out.write("                                <input type=\"password\" class=\"form-control\" id=\"con\" placeholder=\"Ingresar contraseña\" name=\"ptext\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"checkbox\">\r\n");
      out.write("                                <label>\r\n");
      out.write("                                    <input type=\"checkbox\" value=\"\" name=\"check\" checked>Recuérdame\r\n");
      out.write("                                </label>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"modal-center\">\r\n");
      out.write("                                <button type=\"submit\" class=\"btn btco\">Ingresar</button>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- <div class=\"modal-footer\">\r\n");
      out.write("                       <button type=\"submit\" class=\"btn btcon pull-left\" data-dismiss=\"modal\">Cancel</button>\r\n");
      out.write("                       </div> -->\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <!--Modal de registrar-->\r\n");
      out.write("        <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\r\n");
      out.write("            <div class=\"modal-dialog modal-lg\">\r\n");
      out.write("                <div class=\"modal-content mc\">\r\n");
      out.write("                    <div class=\"modal-header \">\r\n");
      out.write("                        <div class=\"modal-center\">\r\n");
      out.write("                            <h5 class=\"text-white text-center modal-title\">BIENVENIDO</h5>                    \r\n");
      out.write("                        </div>\r\n");
      out.write("                        <button type=\"button\" class=\"close fas fa-times\" data-dismiss=\"modal\"></button>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"modal-body\" style=\"padding:40px 50px; \">\r\n");
      out.write("                        <div class=\"form\">\r\n");
      out.write("                            <form action=\"RegistrarCliente\" method=\"post\">\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"n\">Nombres</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"n\" placeholder=\"Ingresar nombres\" name=\"nombre\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"a\">Apellidos</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"a\" placeholder=\"Ingresar nombres\" name=\"apellido\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"t\">Tipo de identificación:</label>\r\n");
      out.write("                                    <select class=\"form-control\" id=\"t\" name=\"tipo\">\r\n");
      out.write("                                        <option selected>Seleccionar tipo de identificación</option>\r\n");
      out.write("                                        <option value=\"Cédula de extranjería\">Cédula de extranjería</option>\r\n");
      out.write("                                        <option value=\"Pasaporte\">Pasaporte</option>\r\n");
      out.write("                                        <option value=\"Cédula de ciudadania\">Cédula de ciudadania</option>\r\n");
      out.write("                                        <option value=\"Tarjeta de identidad\">Tarjeta de identidad</option>\r\n");
      out.write("                                    </select> \r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"row\">\r\n");
      out.write("                                    <div class=\"col-sm-8\">\r\n");
      out.write("                                        <div class=\"form-group\">\r\n");
      out.write("                                            <label for=\"nu\"> N° de identificación</label>\r\n");
      out.write("                                            <input type=\"text\" class=\"form-control\" id=\"nu\" placeholder=\"Ingresar N° de identificación\" name=\"N\">\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <div class=\"col-sm-4\">\r\n");
      out.write("                                        <div class=\"form-group\">\r\n");
      out.write("                                            <label for=\"s\"> Sexo</label>                                            \r\n");
      out.write("                                            <div class=\"form-check\">\r\n");
      out.write("                                                <input class=\"form-check-input radio\" type=\"radio\" id=\"s\" value=\"Hombre\" checked name=\"sexo\">\r\n");
      out.write("                                                <label class=\"l-sb labhom\">Hombre</label>\r\n");
      out.write("                                                <input class=\"form-check-input margin radio\" type=\"radio\" id=\"s\" value=\"Mujer\" name=\"sexo\">\r\n");
      out.write("                                                <label class=\"l-sb labmuj\">Mujer</label>\r\n");
      out.write("                                            </div>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"f\">Fecha de nacimiento</label>\r\n");
      out.write("                                    <input type=\"date\" class=\"form-control\" id=\"f\" name=\"fecha\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"t\">Teléfono</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"t\" placeholder=\"Ingresar teléfono\" name=\"tel\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"d\">Dirección</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"d\" placeholder=\"Ingresar dirección\" name=\"direc\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"de\">Departamento</label>\r\n");
      out.write("                                    <select class=\"custom-select\" id=\"de\" name=\"departamento\">\r\n");
      out.write("                                        <option selected>Seleccionar departamento</option>\r\n");
      out.write("                                        <option value=\"Antioquia\">Antioquia</option>\r\n");
      out.write("                                        <option value=\"Amazonas\">Amazonas</option>\r\n");
      out.write("                                    </select>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"c\">Ciudad</label>\r\n");
      out.write("                                    <select class=\"custom-select\" id=\"c\" name=\"ciudad\">\r\n");
      out.write("                                        <option selected>Seleccionar ciudad</option>\r\n");
      out.write("                                        <option value=\"Medellín\">Medellín</option>\r\n");
      out.write("                                        <option value=\"Bogotá\">Bogotá</option>\r\n");
      out.write("                                    </select>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"coo\">Correo</label>\r\n");
      out.write("                                    <input type=\"email\" class=\"form-control\" id=\"coo\" placeholder=\"Ingresar correo electrónico\" name=\"correo\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"button1-margin text-center\">\r\n");
      out.write("                                    <button type=\"submit\" class=\"btn color-button\">Registrar</button>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <script src=\"js/jquery-3.1.1.min.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap-4-navbar.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("        <script src=\"js/Tables.js\"></script>\r\n");
      out.write("        <script src=\"js/Scroll.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        \r\n");
      out.write("        <section class=\"section1\">\r\n");
      out.write("            <div class=\"container\">\r\n");
      out.write("                <div class=\"flex\">\r\n");
      out.write("                    <div class=\"container\">\r\n");
      out.write("                        <div id=\"demo\" class=\"carousel slide carusel\" data-ride=\"carousel\">\r\n");
      out.write("                            <!-- Carrusel -->\r\n");
      out.write("                            <ul class=\"carousel-indicators \">\r\n");
      out.write("                                <li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\r\n");
      out.write("                                <li data-target=\"#demo\" data-slide-to=\"1\"></li>\r\n");
      out.write("                                <li data-target=\"#demo\" data-slide-to=\"2\"></li>\r\n");
      out.write("                                <li data-target=\"#demo\" data-slide-to=\"3\"></li>\r\n");
      out.write("                                <li data-target=\"#demo\" data-slide-to=\"4\"></li>\r\n");
      out.write("                            </ul>\r\n");
      out.write("                            <!-- The slideshow -->\r\n");
      out.write("                            <div class=\"carousel-inner text-center\">\r\n");
      out.write("                                <div class=\"carousel-item active\">\r\n");
      out.write("                                    <img class=\"imagen1\" src=\"imagenes/foto.png\" alt=\"Los Angeles\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"carousel-item\">\r\n");
      out.write("                                    <img class=\"imagen1\" src=\"imagenes/F11.jpg\" alt=\"Chicago\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"carousel-item\">\r\n");
      out.write("                                    <img class=\"imagen1\" src=\"imagenes/F12.jpg\" alt=\"390\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"carousel-item\">\r\n");
      out.write("                                    <img class=\"imagen1\" src=\"imagenes/F15.jpg\" alt=\"391\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"carousel-item\">\r\n");
      out.write("                                    <img class=\"imagen1\" src=\"imagenes/F18.jpg\" alt=\"392\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- Left and right controls -->\r\n");
      out.write("                            <a class=\"carousel-control-prev prev-at\" href=\"#demo\" data-slide=\"prev\">\r\n");
      out.write("                                <span class=\"carousel-control-prev-icon\"></span>\r\n");
      out.write("                            </a>\r\n");
      out.write("                            <a class=\"carousel-control-next prev-de\" href=\"#demo\" data-slide=\"next\">\r\n");
      out.write("                                <span class=\"carousel-control-next-icon\"></span>\r\n");
      out.write("                            </a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </section>\r\n");
      out.write("\r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/fontawesome-all.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/Footer.css\">\r\n");
      out.write("        <title>JSP Page</title>\r\n");
      out.write("    </head>\r\n");
      out.write("    <body>\r\n");
      out.write("        <footer>\r\n");
      out.write("            <!-- &copy;Gheto 2018 -->\r\n");
      out.write("            <div class=\"Pagos\">\r\n");
      out.write("                <img class=\"icono\" src=\"imagenes/iconos/paypal.ico\" alt=\"Paypal\">\r\n");
      out.write("                <img class=\"icono\" src=\"imagenes/iconos/Master.ico\" alt=\"Master Card\">\r\n");
      out.write("                <img class=\"icono\" src=\"imagenes/iconos/Visa.ico\" alt=\"Visa\">\r\n");
      out.write("                <img class=\"icono\" src=\"imagenes/iconos/Maestro.ico\" alt=\"Maestro\">\r\n");
      out.write("                <!--<img class=\"icono\" src=\"imagenes/iconos/American.ico\" alt=\"American Express\">-->\r\n");
      out.write("                <!--<img class=\"icono\" src=\"imagenes/iconos/Bitcoin.ico\" alt=\"Bitcoin\">-->\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"Personal\">\r\n");
      out.write("                <div class=\"col-6 cor\">\r\n");
      out.write("                    <ul>\r\n");
      out.write("                        <li><a class=\"contenido\" href=\"Nosotros.jsp\"><i class=\"fas fa-users\"></i> Nosotros</a></li>\r\n");
      out.write("                        <li><a class=\"contenido\" href=\"Contacto.jsp\"><i class=\"fas fa-phone\"></i> Contacto</a></li>\r\n");
      out.write("                        <li><a class=\"contenido\" href=\"#\"><i class=\"fas fa-shipping-fast\"></i>Envíos</a></li>\r\n");
      out.write("                        <li><a class=\"contenido\" href=\"#\"><i class=\"fas fa-box-open\"></i>Cambios, Garantías y Devoluciones</a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"Redes\">\r\n");
      out.write("                <div class=\"fila\">\r\n");
      out.write("                    <a class=\"contenido\" href=\"#\"><img class=\"imgicon1\" src=\"imagenes/iconos/Facebook.ico\" alt=\"Facebook\" ></a>\r\n");
      out.write("                    <a class=\"contenido\" href=\"#\"><img class=\"imgicon2\" src=\"imagenes/iconos/Messenger.ico\" alt=\"Messenger\"></a>\r\n");
      out.write("                    <a class=\"contenido\" href=\"#\"><img class=\"imgicon3\" src=\"imagenes/iconos/Twitter.ico\" alt=\"Twitter\"></a>\r\n");
      out.write("                    <a class=\"contenido\" href=\"#\"><img class=\"imgicon4\" src=\"imagenes/iconos/Instagram.ico\" alt=\"Instagram\"></a>\r\n");
      out.write("                    <a class=\"contenido\" href=\"#\"><img class=\"imgicon5\" src=\"imagenes/iconos/Whatsapp.ico\" alt=\"WahtsApp\"></a>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </footer>\r\n");
      out.write("\r\n");
      out.write("        <script src=\"js/jquery-3.1.1.min.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("        <script src=\"js/Scroll.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("            \r\n");
      out.write("        <script src=\"js/jquery-3.1.1.min.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("        <script src=\"js/Scroll.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
