package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class RegistrarInventariosH_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/Navbar.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap-4-navbar.css\" type=\"text/css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/fontawesome-all.min.css\">\r\n");
      out.write("        <title>Ghetto</title>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body class=\"fondo1\">\r\n");
      out.write("        <header class=\"banner\">\r\n");
      out.write("        </header>\r\n");
      out.write("        ");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <meta charset=\"utf-8\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap-4-navbar.css\" type=\"text/css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/fontawesome-all.min.css\">\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/jquery.bootstrap4.css\" type=\"text/css\" />\r\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/hombre.css\" type=\"text/css\">\r\n");
      out.write("\r\n");
      out.write("        <title>Ghetto</title>\r\n");
      out.write("    </head>\r\n");
      out.write("\r\n");
      out.write("    <body class=\"fondo1\">\r\n");
      out.write("\r\n");
      out.write("        <nav class=\"navbar navbar-expand-lg navbar-dark mynavbar\" data-toggle=\"sticky-onscroll\">\r\n");
      out.write("            <a class=\"navbar-brand\" href=\"Index.jsp\">Ghetto</a>\r\n");
      out.write("            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("                <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("            </button>\r\n");
      out.write("            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n");
      out.write("                <ul class=\"navbar-nav mx-auto\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Hombre\">HOMBRE</a> </li>\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Mujer.jsp\">MUJER</a> </li>\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Nosotros.jsp\">EL BARRIO</a> </li>\r\n");
      out.write("\r\n");
      out.write("                    ");

                        HttpSession se = request.getSession(true);
                        String rol = "invitado";
                        String nu = "Ingresar";
                        String msg = "Registrar";
                        if (se.getAttribute("rusuario") == null) {
                            rol = "3";
                        } else {
                            rol = (String) se.getAttribute("rusuario");
                            nu = (String) se.getAttribute("nusuario");
                            msg = "";
                        }
                        if (rol.equals("1")) {

                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Empleados </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarEmpleados.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarEmpleado\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Clientes </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarClientes.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarCliente\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Pedidos </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarPedidos.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarPedido\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Ventas </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarVentas.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarVenta\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("                            Inventarios\r\n");
      out.write("                        </a>\r\n");
      out.write("                        <ul class=\"dropdown-menu m\" aria-labelledby=\"navbarDropdownMenuLink\">\r\n");
      out.write("                            <li><a class=\"dropdown-item d dropdown-toggle\" href=\"#\">Hombres</a>\r\n");
      out.write("                                <ul class=\"dropdown-menu m\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"RegistrarInventariosH.jsp\">Registrar</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"ListarInventarioH\">Listar</a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                            <li><a class=\"dropdown-item d dropdown-toggle\" href=\"#\">Mujeres</a>\r\n");
      out.write("                                <ul class=\"dropdown-menu m\">\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"RegistrarInventariosM.jsp\">Registrar</a></li>\r\n");
      out.write("                                    <li><a class=\"dropdown-item d\" href=\"ListarInventarioM\">Listar</a></li>\r\n");
      out.write("                                </ul>\r\n");
      out.write("                            </li>\r\n");
      out.write("                        </ul>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <!--<li class=\"nav-item dropdown\">-->\r\n");
      out.write("                    <!--    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Facturaciones </a>-->\r\n");
      out.write("                    <!--    <div class=\"dropdown-menu m\">-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Registrar</a>-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Listar</a>-->\r\n");
      out.write("                    <!--    </div>-->\r\n");
      out.write("                    <!--</li>-->\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Proveedores </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarProveedores.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarProveedor\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Contactos </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"Contacto.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarContacto\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <!--<li class=\"nav-item dropdown\">-->\r\n");
      out.write("                    <!--    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Mensajería </a>-->\r\n");
      out.write("                    <!--    <div class=\"dropdown-menu m\">-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Registrar</a>-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Listar</a>-->\r\n");
      out.write("                    <!--    </div>-->\r\n");
      out.write("                    <!--</li>-->\r\n");
      out.write("                    <!--<li class=\"nav-item dropdown\">-->\r\n");
      out.write("                    <!--    <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Pagos </a>-->\r\n");
      out.write("                    <!--    <div class=\"dropdown-menu m\">-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Registrar</a>-->\r\n");
      out.write("                    <!--        <a class=\"dropdown-item d\" href=\"#\">Listar</a>-->\r\n");
      out.write("                    <!--    </div>-->\r\n");
      out.write("                    <!--</li>-->\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Audirotías </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"Permisos.jsp\">Permisos</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"Logs.jsp\">Logs</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    ");
                     
                        } else if (rol.equals("2")) {
                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Pedidos </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"RegistrarPedidos.jsp\">Registrar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarPedido\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    <li class=\"nav-item dropdown\">\r\n");
      out.write("                        <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbardrop\" data-toggle=\"dropdown\"> Ventas </a>\r\n");
      out.write("                        <div class=\"dropdown-menu m\">                            \r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarVentas.jsp\">Listar</a>\r\n");
      out.write("                            <a class=\"dropdown-item d\" href=\"ListarVenta\">Listar</a>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </li>\r\n");
      out.write("                    ");

                    } else if (rol.equals("3")) {

                    
      out.write("\r\n");
      out.write("                    <i class=\"fas fa-shopping-cart icon-cart\">\r\n");
      out.write("                        <div class=\"badge badge-color\" id=\"badge-cart\">0</div>\r\n");
      out.write("                    </i>\r\n");
      out.write("                    ");
                    } else if (rol.equals("4")) {

                    
      out.write("\r\n");
      out.write("                    <i class=\"fas fa-shopping-cart icon-cart\">\r\n");
      out.write("                        <div class=\"badge badge-color\" id=\"badge-cart\">0</div>\r\n");
      out.write("                    </i>\r\n");
      out.write("                    ");
                        }
                    
      out.write("\r\n");
      out.write("\r\n");
      out.write("                </ul>\r\n");
      out.write("\r\n");
      out.write("                ");

                    if (se.getAttribute("rusuario") == null) {
                
      out.write("\r\n");
      out.write("                <ul class=\"navbar-nav\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" data-toggle=\"modal\" data-target=\"#myModal1\"> Ingresar  <span class=\"fas fa-sign-in-alt \"></span> </a></li>\r\n");
      out.write("\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link myitem\" href=\"#\" data-toggle=\"modal\" data-target=\"#myModal2\">Registrar</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                ");

                } else if (se.getAttribute("4") == null){
                
      out.write("\r\n");
      out.write("                <ul class=\"navbar-nav\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"#\"> Mis pedidos <span class=\"fas fas fa-dolly \"></span> </a></li>\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Logout\"> ");
      out.print( nu);
      out.write("</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                ");
     
                    } else {
                
      out.write("\r\n");
      out.write("                  \r\n");
      out.write("                <ul class=\"navbar-nav\">\r\n");
      out.write("                    <li class=\"nav-item\"> <a class=\"nav-link\" href=\"Logout\"> ");
      out.print( nu);
      out.write("</a></li>\r\n");
      out.write("                </ul>\r\n");
      out.write("                ");

                    }
                
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("        </nav>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <!--Modal de ingresar-->\r\n");
      out.write("        <div class=\"modal fade\" id=\"myModal1\" role=\"dialog\">\r\n");
      out.write("            <div class=\"modal-dialog\">\r\n");
      out.write("                <div class=\"modal-content mc\">\r\n");
      out.write("                    <div class=\"modal-header \">\r\n");
      out.write("                        <button type=\"button\" class=\"close fas fa-times\" data-dismiss=\"modal\"></button>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"modal-body\" style=\"padding:40px 50px;\">\r\n");
      out.write("                        <form action=\"Login\">\r\n");
      out.write("\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"correo\">Correo electrónico:</label>\r\n");
      out.write("                                <input type=\"email\" class=\"form-control\" id=\"correo\" placeholder=\"Ingresar correo electrónico\" name=\"ntext\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"form-group\">\r\n");
      out.write("                                <label for=\"con\">Contraseña:</label>\r\n");
      out.write("                                <input type=\"password\" class=\"form-control\" id=\"con\" placeholder=\"Ingresar contraseña\" name=\"ptext\">\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"checkbox\">\r\n");
      out.write("                                <label>\r\n");
      out.write("                                    <input type=\"checkbox\" value=\"\" name=\"check\" checked>Recuérdame\r\n");
      out.write("                                </label>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <div class=\"modal-center\">\r\n");
      out.write("                                <button type=\"submit\" class=\"btn btco\">Ingresar</button>\r\n");
      out.write("                            </div>\r\n");
      out.write("                        </form>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <!-- <div class=\"modal-footer\">\r\n");
      out.write("                       <button type=\"submit\" class=\"btn btcon pull-left\" data-dismiss=\"modal\">Cancel</button>\r\n");
      out.write("                       </div> -->\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <!--Modal de registrar-->\r\n");
      out.write("        <div class=\"modal fade\" id=\"myModal2\" role=\"dialog\">\r\n");
      out.write("            <div class=\"modal-dialog modal-lg\">\r\n");
      out.write("                <div class=\"modal-content mc\">\r\n");
      out.write("                    <div class=\"modal-header \">\r\n");
      out.write("                        <div class=\"modal-center\">\r\n");
      out.write("                            <h5 class=\"text-white text-center modal-title\">BIENVENIDO</h5>                    \r\n");
      out.write("                        </div>\r\n");
      out.write("                        <button type=\"button\" class=\"close fas fa-times\" data-dismiss=\"modal\"></button>\r\n");
      out.write("                    </div>\r\n");
      out.write("\r\n");
      out.write("                    <div class=\"modal-body\" style=\"padding:40px 50px; \">\r\n");
      out.write("                        <div class=\"form\">\r\n");
      out.write("                            <form action=\"RegistrarCliente\" method=\"post\">\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"n\">Nombres</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"n\" placeholder=\"Ingresar nombres\" name=\"nombre\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"a\">Apellidos</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"a\" placeholder=\"Ingresar nombres\" name=\"apellido\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"t\">Tipo de identificación:</label>\r\n");
      out.write("                                    <select class=\"form-control\" id=\"t\" name=\"tipo\">\r\n");
      out.write("                                        <option selected>Seleccionar tipo de identificación</option>\r\n");
      out.write("                                        <option value=\"Cédula de extranjería\">Cédula de extranjería</option>\r\n");
      out.write("                                        <option value=\"Pasaporte\">Pasaporte</option>\r\n");
      out.write("                                        <option value=\"Cédula de ciudadania\">Cédula de ciudadania</option>\r\n");
      out.write("                                        <option value=\"Tarjeta de identidad\">Tarjeta de identidad</option>\r\n");
      out.write("                                    </select> \r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"row\">\r\n");
      out.write("                                    <div class=\"col-sm-8\">\r\n");
      out.write("                                        <div class=\"form-group\">\r\n");
      out.write("                                            <label for=\"nu\"> N° de identificación</label>\r\n");
      out.write("                                            <input type=\"text\" class=\"form-control\" id=\"nu\" placeholder=\"Ingresar N° de identificación\" name=\"N\">\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                    <div class=\"col-sm-4\">\r\n");
      out.write("                                        <div class=\"form-group\">\r\n");
      out.write("                                            <label for=\"s\"> Sexo</label>                                            \r\n");
      out.write("                                            <div class=\"form-check\">\r\n");
      out.write("                                                <input class=\"form-check-input radio\" type=\"radio\" id=\"s\" value=\"Hombre\" checked name=\"sexo\">\r\n");
      out.write("                                                <label class=\"l-sb labhom\">Hombre</label>\r\n");
      out.write("                                                <input class=\"form-check-input margin radio\" type=\"radio\" id=\"s\" value=\"Mujer\" name=\"sexo\">\r\n");
      out.write("                                                <label class=\"l-sb labmuj\">Mujer</label>\r\n");
      out.write("                                            </div>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"f\">Fecha de nacimiento</label>\r\n");
      out.write("                                    <input type=\"date\" class=\"form-control\" id=\"f\" name=\"fecha\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"t\">Teléfono</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"t\" placeholder=\"Ingresar teléfono\" name=\"tel\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"d\">Dirección</label>\r\n");
      out.write("                                    <input type=\"text\" class=\"form-control\" id=\"d\" placeholder=\"Ingresar dirección\" name=\"direc\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"de\">Departamento</label>\r\n");
      out.write("                                    <select class=\"custom-select\" id=\"de\" name=\"departamento\">\r\n");
      out.write("                                        <option selected>Seleccionar departamento</option>\r\n");
      out.write("                                        <option value=\"Antioquia\">Antioquia</option>\r\n");
      out.write("                                        <option value=\"Amazonas\">Amazonas</option>\r\n");
      out.write("                                    </select>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"c\">Ciudad</label>\r\n");
      out.write("                                    <select class=\"custom-select\" id=\"c\" name=\"ciudad\">\r\n");
      out.write("                                        <option selected>Seleccionar ciudad</option>\r\n");
      out.write("                                        <option value=\"Medellín\">Medellín</option>\r\n");
      out.write("                                        <option value=\"Bogotá\">Bogotá</option>\r\n");
      out.write("                                    </select>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"form-group\">\r\n");
      out.write("                                    <label for=\"coo\">Correo</label>\r\n");
      out.write("                                    <input type=\"email\" class=\"form-control\" id=\"coo\" placeholder=\"Ingresar correo electrónico\" name=\"correo\">\r\n");
      out.write("                                </div>\r\n");
      out.write("                                <div class=\"button1-margin text-center\">\r\n");
      out.write("                                    <button type=\"submit\" class=\"btn color-button\">Registrar</button>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </form>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <script src=\"js/jquery-3.1.1.min.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap-4-navbar.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("        <script src=\"js/Tables.js\"></script>\r\n");
      out.write("        <script src=\"js/Scroll.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("        <div class=\"container\">\r\n");
      out.write("            <div class=\"form\">\r\n");
      out.write("                <h2 class=\"myh2\">Registrar Inventario de Hombre</h2>\r\n");
      out.write("                <form action=\"RegistrarInvH\" method=\"post\" enctype=\"multipart/form-data\">\r\n");
      out.write("                    <div class=\"form-group row\">\r\n");
      out.write("                        <label for=\"ite\" class=\"col-sm-2 col-form-label l-sb\">Item</label>\r\n");
      out.write("                        <div class=\"col-sm-10\">\r\n");
      out.write("                            <input type=\"hidden\" class=\"form-control\" id=\"ite\" placeholder=\"Ingresar nombre\" name=\"item\">\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"form-group row\">\r\n");
      out.write("                        <label for=\"n\" class=\"col-sm-2 col-form-label l-sb\">Nombre</label>\r\n");
      out.write("                        <div class=\"col-sm-10\">\r\n");
      out.write("                            <input type=\"text\" class=\"form-control\" id=\"n\" placeholder=\"Ingresar nombre\" name=\"nombre\">\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div> \r\n");
      out.write("                    \r\n");
      out.write("                    <div class=\"form-group row\">\r\n");
      out.write("                        <label for=\"text\" class=\"col-sm-2 col-form-label l-sb\">Descripción:</label>\r\n");
      out.write("                        <div class=\"col-sm-10\">\r\n");
      out.write("                            <textarea class=\"form-control\" id=\"text\" rows=\"3\" placeholder=\"Ingresar descripción\"  name=\"descripcion\"></textarea>\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"form-group row\">\r\n");
      out.write("                        <label for=\"ca\" class=\"col-sm-2 col-form-label l-sb\">Precio</label>\r\n");
      out.write("                        <div class=\"col-sm-10\">\r\n");
      out.write("                            <input type=\"text\" class=\"form-control\" id=\"ca\" placeholder=\"Ingresar precio\" name=\"precio\">\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"form-group row\">\r\n");
      out.write("                        <label for=\"correo\" class=\"col-sm-2 col-form-label l-sb\">Colección:</label>\r\n");
      out.write("                        <div class=\"col-sm-10\">\r\n");
      out.write("                            <input type=\"text\" class=\"form-control\" id=\"correo\" placeholder=\"Ingresar colección\"  name=\"coleccion\">\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"form-group row\">\r\n");
      out.write("                        <label for=\"img\" class=\"col-sm-2 col-form-label l-sb\">Imagen</label>\r\n");
      out.write("                        <div class=\"col-sm-10\">\r\n");
      out.write("                            <input type=\"file\" class=\"form-control\" id=\"img\" name=\"imagen\">\r\n");
      out.write("                        </div>\r\n");
      out.write("                    </div>\r\n");
      out.write("                    <div class=\"button-principal\">\r\n");
      out.write("                        <button type=\"submit\" class=\"btn btn-color\">Registar Inventario</button>\r\n");
      out.write("                    </div>\r\n");
      out.write("                </form>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("        <script src=\"js/jquery-3.1.1.min.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap-4-navbar.js\"></script>\r\n");
      out.write("        <script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("        <script src=\"js/Scroll.js\"></script>\r\n");
      out.write("    </body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
