<%-- 
    Document   : Hombre
    Created on : 13/11/2018, 07:08:21 PM
    Author     : Tamayo B
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Controlador.Modelo.InvHombre"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-4-navbar.css" type="text/css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/hombre.css" type="text/css">

        <title>Ghetto</title>
    </head>

    <body class="fondo1">
        <header class="banner">

        </header>


        <!--   <nav class="navbar navbar-expand-lg navbar-dark mynavbar" data-toggle="sticky-onscroll">
            <a class="navbar-brand" href="index.html">Ghetto</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
    
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item"> <a class="nav-link" href="Hombre.jsp">HOMBRE</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="Mujer.jsp">MUJER</a> </li>
                    <li class="nav-item"> <a class="nav-link" href="Nosotros.jsp">EL BARRIO</a> </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item"> <a class="nav-link" data-toggle="modal" data-target="#myModal">Cerrar sesión  <span class="fas fa-sign-out-alt "></span> </a></li>
                </ul>
            </div>
        </nav> -->

        <%@include file="Navbar.jsp" %>

        <%            ArrayList<InvHombre> al = (ArrayList) request.getAttribute("lista");
        %>

        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-2" style="height: 100%;">
                    <div class="catalogo">
                        <h3>Catálogo</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label label-items" for="customCheck1">Camisas</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label label-items" for="customCheck2">Pantalones</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck3">
                            <label class="custom-control-label label-items" for="customCheck3">Chaquetas</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck4">
                            <label class="custom-control-label label-items" for="customCheck4">Zapatos</label>
                        </div>
                    </div>
                    <div class="catalogo mt-1">
                        <h3>Talla</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck5">
                            <label class="custom-control-label label-items" for="customCheck5">XL</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck6">
                            <label class="custom-control-label label-items" for="customCheck6">L</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck7">
                            <label class="custom-control-label label-items" for="customCheck7">M</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck8">
                            <label class="custom-control-label label-items" for="customCheck8">S</label>
                        </div>
                    </div>
                </div>

                <%
                    for (InvHombre Invh : al) {
                %>
                <!-- <div class="col-lg-4">
                      <div class="card-deck">
                          <div class="card mb-3">
                              <div class="container-1">
                                    <img src="imagenes/<%= Invh.getImgNom()%>" alt="Avatar" class="image" >
                                    <div class="overlay">
                                        <a class="icon">
                                             <i class="far fa-eye fa-2x icon pointer" data-toggle="modal" data-target="#myModal100" onclick="ver('<%= Invh.getDescripcion()%>',<%= Invh.getItem()%>, '<%= Invh.getPrecio()%>'); mostrar(<%= Invh.getItem()%>, '<%= Invh.getNombre()%>', '<%= Invh.getPrecio()%>')"></i>
                                         </a>
                                    </div>
                              </div>
                              <div class="card-body">
                                  <h5 class="card-title"><%= Invh.getNombre()%></h5>
                                  <h5 class="card-title"><%= Invh.getColeccion()%></h5>
                                  <p class="card-text">$<%= Invh.getPrecio()%></p>
                              </div>
                          </div>
                      </div>
                 </div> -->

                <!-- <div class="col-lg-3 mb-4">  
                     <div class="card mb-3 overlay " style="width: 18rem;">
                              <a class="pointer" data-toggle="modal" data-target="#myModal100" type="button"  onclick="ver('<%= Invh.getDescripcion()%>',<%= Invh.getItem()%>, '<%= Invh.getPrecio()%>'); mostrar(<%= Invh.getItem()%>, '<%= Invh.getNombre()%>', '<%= Invh.getPrecio()%>')">
                                  <div class="image-card">
                                      <img class="card-img-top" src="imagenes/<%= Invh.getImgNom()%>" alt="Card image cap ">
                                  </div>
                              </a>
                         <div class="card-body">
                             <p class="card-text paragraft"><%= Invh.getNombre()%></p>
                             <p class="card-text paragraft"><%= Invh.getColeccion()%></p>
                             <p class="card-text paragraft">$<%= Invh.getPrecio()%></p>
                         </div>
                     </div>
                 </div> -->

                <div class="col-lg-3 mb-4">
                    <div class="card-deck"> 
                        <div class="card mb-3" style="width: 18rem;">

                            <div class="container-1">
                                <img src="imagenes/<%= Invh.getImgNom()%>" alt="Avatar" class="image" >
                                <div class="overlay">
                                    <a class="icon">
                                        <i class="far fa-eye fa-2x icon pointer" data-toggle="modal" data-target="#myModal100" onclick="ver('<%= Invh.getDescripcion()%>',<%= Invh.getItem()%>, <%= Invh.getPrecio()%>, document.getElementById('img').src = 'imagenes/<%= Invh.getImgNom()%>'); mostrar(<%= Invh.getItem()%>, '<%= Invh.getNombre()%>', '<%= Invh.getPrecio()%>')"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="card-body">
                                <p class="card-text paragraft"><%= Invh.getNombre()%></p>
                                <p class="card-text paragraft"><%= Invh.getColeccion()%></p>
                                <p class="card-text paragraft">$<%= Invh.getPrecio()%></p>
                            </div>
                        </div>
                    </div>
                </div>




                <%
                    }
                %>

            </div>
        </div>
        <div class="modal fade" id="myModal100" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content mc">
                    <div class="modal-header mo">
                        <button type="button" class="close fas fa-times" data-dismiss="modal"></button>
                    </div>
                    <div class="modal-body" style="padding:25px 20px;">
                        <div class="col-lg-6 float-margin">
                            <img id="img"  alt="Avatar" class="image12">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 float">
                            <h3 class="l-sb" id="des"></h3>
                            <label class="l-sb tama-letra">Item:</label>
                            <label class="tama-letra" id="ite"></label>
                            <h4 class="l-sb" id="pre">$</h4>

                            <form action="Cart" method="post">
                                <label for="talla">Talla</label>
                                <div class="bottom">
                                    <select class="custom-select" id="talla" name="tipo" style="width:190px;">
                                        <option selected>Elige una opción</option>
                                        <option value="XL">XL</option>
                                        <option value="L">L</option>
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                    </select>
                                </div>
                                <input type="hidden" id="id"  name="id">
                                <input type="hidden" id="nom"  name="nombre">
                                <input type="hidden" id="precio"  name="precio">
                                <label for="cantidad">Cantidad</label>
                                <input type="text" class="form-control col-sm-2 text-center" id="cantidad" value="<%= '1'%>" name="cantidad">
                                <div class="top">
                                    <button class="btn btcon" type="submit" id="btn-cart">Añadir al carrito</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>











        <%@include file="Footer.jsp" %>

        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap-4-navbar.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/hombre.js"></script>
        <script src="js/Scroll.js"></script>
        <script>
                                           function mostrar(i, n, p) {
                                               document.getElementById('myModal100').style.display = 'block';
                                               document.getElementById('id').value = i;
                                               document.getElementById('nom').value = n;
                                               document.getElementById('precio').value = p;
                                           }
        </script>
        <script>
            function ver(descripcion, item, precio) {
                document.getElementById('myModal100').style.display = 'block';
                document.getElementById('des').innerHTML = descripcion;
                document.getElementById('ite').innerHTML = item;
                document.getElementById('pre').innerHTML = precio;
            }
        </script>
    </body>


</html>