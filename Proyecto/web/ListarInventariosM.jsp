<%-- 
    Document   : ListarInventariosM
    Created on : 13/11/2018, 07:31:41 PM
    Author     : Tamayo B
--%>

<%@page import="Controlador.Modelo.InvMujer"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap-4-navbar.css" type="text/css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/jquery.bootstrap4.css" type="text/css" />
        <title>Ghetto</title>
    </head>

    <body class="fondo1">
        <header class="banner">
        </header>
        <%@include file="Navbar.jsp" %>
        <%

            ArrayList<InvMujer> as = (ArrayList) request.getAttribute("Lise");

        %>
        <div class="container cont-table">
            <h2 class="myh2">Listar inventario de Mujer</h2>
            <table id="table_id" class="table table-hover mytable">
                <thead class="thead tableTT">
                    <tr>
                        <th scope="col">Item</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Colecci�n</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <%                        System.out.println(as);
                        for (InvMujer Invm : as) {

                    %>
                    <tr>
                        <td><%=Invm.getItem()%></td>
                        <td><%=Invm.getNombre()%></td>
                        <td><%=Invm.getColeccion()%></td>
                        <td>
                            <a class="btn btn-wh btn-sm" data-toggle="modal" data-target="#myModal" onclick="ver( '<%= Invm.getDescripcion() %>', <%= Invm.getPrecio()%>)"><span class="far fa-eye" aria-hidden="true"></span></a>
                            <a class="btn btn-bl btn-sm" data-toggle="modal" data-target="#myModal10" onclick="Mostrar(<%= Invm.getItem()%>, '<%= Invm.getNombre()%>', '<%= Invm.getDescripcion()%>', '<%= Invm.getColeccion()%>', <%= Invm.getPrecio()%>)"><span class="fas fa-edit h" aria-hidden="true"></span></a>
                            <a class="btn btn-re btn-sm tamb"><span class="fas fa-times h" aria-hidden="true"></span></a> 
                        </td>
                    </tr>
                    <%
                        }
                    %>
                </tbody>
            </table>
        </div>

        <!--Ver m�s detalles-->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content mc">
                    <div class="modal-header mo">
                        <button type="button" class="close fas fa-times" data-dismiss="modal"></button>
                    </div>
                    <div class="modal-body" style="padding:40px 50px;">
                        <div class="form-group">
                            <label class="l-sb">Descripci�n</label>
                            <label id="descr"></label>
                        </div>
                        <div class="form-group">
                            <label class="l-sb">Precio:</label>
                            <label id="precio"></label>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>


        <!--Modificar-->
        <div class="modal fade" id="myModal10" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content mc">
                    <div class="modal-header mo">
                        <button type="button" class="close fas fa-times" data-dismiss="modal"></button>
                    </div>
                    <div class="modal-body" style="padding:40px 50px;">
                        <form action="ActualizarInvM">
                            <div class="form-group row">
                                <label for="id" class="col-sm-2 col-form-label">Item:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="item" name="item" readonly="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="categ" class="col-sm-2 col-form-label">Nombre:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nom" placeholder="Ingresar nombre" name="nombre">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="desc" class="col-sm-2 col-form-label">Descripci�n:</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="desc" rows="3" placeholder="Ingresar descripci�n" name="descripcion"></textarea>
                                </div>
                            </div>   
                            
                            <div class="form-group row">
                                <label for="colec" class="col-sm-2 col-form-label">Colecci�n:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="coleccion" placeholder="Ingresar colecci�n" name="coleccion">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pre" class="col-sm-2 col-form-label">Precio:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="pre" placeholder="Ingresar precio" name="precio" >
                                </div>
                            </div>
                            <div class="button-principal">
                                <button type="submit" class="btn btn-mod">Guardar modificaci�n</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function ver(descripcion, precio) {
                document.getElementById('myModal').style.display = 'block';
                document.getElementById('descr').innerHTML = descripcion;
                document.getElementById('precio').innerHTML = precio;
            }
        </script>
        <script>
            function Mostrar(Item, nombre, desc, colec, pre) {
                document.getElementById('myModal10').style.display = 'block';
                document.getElementById('item').value = Item;
                document.getElementById('nom').value = nombre;                
                document.getElementById('desc').value = desc;               
                document.getElementById('coleccion').value = colec;
                document.getElementById('pre').value = pre;
            }
        </script>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap-4-navbar.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.dataTables.js"></script>
        <script src="js/dataTables.bootstrap4.js"></script>
        <script src="js/Tables.js"></script>
        <script src="js/Scroll.js"></script>
    </body>



</html>

