<%-- 
    Document   : Mujer
    Created on : 13/11/2018, 06:58:14 PM
    Author     : Tamayo B
--%>

<%@page import="Controlador.Modelo.InvMujer"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap-4-navbar.css" type="text/css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/mujer.css">
        <title>Ghetto</title>
    </head>

    <body class="fondo1">
        <header class="banner">
            <i id="theFixed" class="fas fa-shopping-cart icon-cart">
                <div class="badge badge-color" id="badge-cart">0</div>
            </i>
        </header>

        <!--  <nav class="navbar navbar-expand-lg navbar-dark mynavbar" data-toggle="sticky-onscroll">
              <a class="navbar-brand" href="index.jsp">Ghetto</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
      
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mx-auto">
                      <li class="nav-item"> <a class="nav-link" href="Hombre.jsp">HOMBRE</a> </li>
                      <li class="nav-item"> <a class="nav-link" href="Mujer.jsp">MUJER</a> </li>
                      <li class="nav-item"> <a class="nav-link" href="Nosotros.jsp">EL BARRIO</a> </li>
                  </ul>
                  <ul class="navbar-nav">
                      <li class="nav-item"> <a class="nav-link" data-toggle="modal" data-target="#myModal">Cerrar sesión  <span class="fas fa-sign-out-alt "></span> </a></li>
                  </ul>
              </div>
          </nav> -->
        <%@include file="Navbar.jsp" %>
        <%        ArrayList<InvMujer> al = (ArrayList) request.getAttribute("lista");
        %>

        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-2">
                    <div class="catalogo">
                        <h3>Catálogo</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                            <label class="custom-control-label label-items" for="customCheck1">Top's</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck1.1">
                            <label class="custom-control-label label-items" for="customCheck1.1">Joggers</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck3">
                            <label class="custom-control-label label-items" for="customCheck3">Camisas</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck4">
                            <label class="custom-control-label label-items" for="customCheck4">Faldas</label>
                        </div>
                    </div>
                    <div class="catalogo mt-1">
                        <h3>Talla</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck5">
                            <label class="custom-control-label label-items" for="customCheck5">XL</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck6">
                            <label class="custom-control-label label-items" for="customCheck6">XS</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck7">
                            <label class="custom-control-label label-items" for="customCheck7">S</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck8">
                            <label class="custom-control-label label-items" for="customCheck8">M</label>
                        </div>
                    </div>
                </div>
                <%
                    for (InvMujer Invm : al) {
                %>
                <div class="col-lg-10">
                    <div class="card-deck">
                        <div class="card mb-3">
                            <div class="container-1">
                                <img src="#" alt="Avatar" class="image"  width="284.667">
                                <div class="overlay">
                                    <a class="icon">
                                        <i class="far fa-eye fa-2x icon pointer" data-toggle="modal" data-target="#myModal100" onclick="ver('<%= Invm.getDescripcion()%>',<%= Invm.getItem()%>, '<%= Invm.getPrecio()%>'); mostrar(<%= Invm.getItem()%>, '<%= Invm.getNombre()%>', '<%= Invm.getPrecio()%>')"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><%= Invm.getNombre()%></h5>
                                <h5 class="card-title"><%= Invm.getColeccion()%></h5>
                                <p class="card-text">$<%= Invm.getPrecio()%></p>
                            </div>
                        </div>               
                    </div>                                
                </div>
                <%
                    }
                %>
            </div>
        </div>

        <div class="modal fade" id="myModal100" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content mc">
                    <div class="modal-header mo">
                        <button type="button" class="close fas fa-times" data-dismiss="modal"></button>
                    </div>
                    <div class="modal-body" style="padding:25px 20px;">
                        <div class="col-lg-6 float-margin">
                            <img id="img" alt="Avatar" class="image12">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 float">
                            <h3 class="l-sb" id="des"></h3>
                            <label class="l-sb tama-letra">Item:</label>
                            <label class="tama-letra" id="ite"></label>
                            <h4 class="l-sb" id="pre">$</h4>

                            <form action="Cart" method="post">
                                <label for="talla">Talla</label>
                                <div class="bottom">
                                    <select class="custom-select" id="talla" name="tipo" style="width:190px;">
                                        <option selected>Elige una opción</option>
                                        <option value="XL">XL</option>
                                        <option value="L">L</option>
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                    </select>
                                </div>
                                <input type="text" id="id"  name="id">
                                <input type="text" id="nom"  name="nombre">
                                <input type="text" id="precio"  name="precio">
                                <label for="cantidad">Cantidad</label>
                                <input type="text" class="form-control col-sm-2 text-center" id="cantidad" value="<%= '1'%>" name="cantidad">
                                <div class="top">
                                    <button class="btn btcon" type="submit" id="btn-cart">Añadir al carrito</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <%@include file="Footer.jsp" %>

        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap-4-navbar.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/hombre.js"></script>
        <script src="js/Scroll.js"></script>
        <script>
            function mostrar(i, n, p) {
                document.getElementById('myModal100').style.display = 'block';
                document.getElementById('id').value = i;
                document.getElementById('nom').value = n;
                document.getElementById('precio').value = p;
            }
        </script>
        <script>
            function ver(descripcion, item, precio, imagen) {
                document.getElementById('myModal100').style.display = 'block';
                document.getElementById('des').innerHTML = descripcion;
                document.getElementById('ite').innerHTML = item;
                document.getElementById('pre').innerHTML = precio;
                document.getElementById('img').innerHTML = imagen;
            }
        </script>
    </body>
</html>

